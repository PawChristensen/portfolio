package test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import application.model.Order;
import application.model.OrderLine;
import application.model.PriceList;
import application.model.PriceListLine;
import application.model.TourBooking;
import application.model.products.Tour;


public class TourBookingTest {

	private TourBooking tourBooking;
	private OrderLine orderLine;
	private OrderLine orderLine2;
	
	@Before
	public void setUp() throws Exception {
		Tour tour = new Tour("Rundvisning", "En times rundvisning med smagsprøver efterfulgt", 10);
		PriceList butik = new PriceList("Butik");
		PriceListLine pll = butik.createPriceListLine(100, tour);
		Order o1 = new Order(LocalDateTime.of(2017, 1, 1, 13, 30), butik);
		orderLine = o1.createOrderLine(pll.getProduct(), 10);
		orderLine2 = o1.createOrderLine(pll.getProduct(), 15);
		
		tourBooking = new TourBooking(LocalDateTime.of(1999, 01, 01, 13, 00), orderLine);
	}

	@Test
	public void TC1Constructor() {
		LocalDateTime datetime = tourBooking.getDate();
		OrderLine ol = tourBooking.getOrderLine();
		
		assertEquals(LocalDateTime.of(1999, 01, 01, 13, 00),datetime);	
		assertEquals(orderLine,ol);
	}
	
	@Test
	public void setDateTC2() {
		tourBooking.setDate(LocalDateTime.of(2000, 01, 01, 13, 00));
		LocalDateTime expected = LocalDateTime.of(2000, 01, 01, 13, 00);
		LocalDateTime actual = tourBooking.getDate();
		assertEquals(expected,actual);
	}
	
	@Test
	public void setOrderLineTC3() {	
		tourBooking.setOrderLine(orderLine2);
		OrderLine expected = orderLine2;
		OrderLine actual = tourBooking.getOrderLine();
		assertEquals(expected,actual);
	}

	
}
