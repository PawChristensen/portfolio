package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import application.model.products.Glass;

public class GlassTest {
	Glass object;

	@Before
	public void setUp() throws Exception {
		object = new Glass("Ølglas","Man kan drikke øl af det", 10);
	}

	@Test
	public void TC1Constructor() {
		String name = object.getName();
		String description = object.getDescription();
		int inStock = object.getInStock();
		assertEquals("Ølglas",name);
		assertEquals("Man kan drikke øl af det",description);
		assertEquals(10,inStock);
	}

}
