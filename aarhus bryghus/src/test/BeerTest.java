package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import application.model.products.Beer;

public class BeerTest {
	Beer object;

	@Before
	public void setUp() throws Exception {
		object = new Beer("Klosterbryg", "En dejlig øl", 10, 60, 4.7);
	}

	@Test
	public void TC1Constructor() {
		String name = object.getName();
		String description = object.getDescription();
		int inStock = object.getInStock();
		double cl = object.getCl();
		double alcohol = object.getAlcohol();

		assertEquals("Klosterbryg", name);
		assertEquals("En dejlig øl", description);
		assertEquals(10, inStock);
		assertEquals(60, cl, 0.0001);
		assertEquals(4.7, alcohol, 0.0001);
	}

	@Test
	public void toStringTC2() {
		String expected = "Klosterbryg flaske, 60.0 cl, 4.7%";
		String actual = object.toString();
		assertEquals(expected, actual);
	}

	@Test
	public void toStringTC3() {
		object.setCl(50);
		String expected = "Klosterbryg flaske, 50.0 cl, 4.7%";
		String actual = object.toString();
		assertEquals(expected, actual);
	}

	@Test
	public void toStringTC4() {
		object.setCl(40);
		String expected = "Klosterbryg fadøl, 40.0 cl, 4.7%";
		String actual = object.toString();
		assertEquals(expected, actual);
	}

}
