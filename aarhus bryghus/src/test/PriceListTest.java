package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import application.model.PriceList;
import application.model.PriceListLine;
import application.model.Product;

public class PriceListTest {

	private PriceList object;
	private Product product;
	private PriceListLine line;

	@Before
	public void setUp() throws Exception {
		object = new PriceList("Fredagsbar");
		product = new Product("", "", 2);
		line = object.createPriceListLine(10, product);
	}

	@Test
	public void TC1Constructor() {
		assertEquals("Fredagsbar", object.getTitle());
		assertNotNull(object.getLstPriceListLines());
	}

	@Test
	public void createPriceListLineTC2() {
		PriceListLine line2 = object.createPriceListLine(120, product);

		ArrayList<PriceListLine> tester = new ArrayList<PriceListLine>();
		tester.add(line);
		tester.add(line2);
		assertEquals(tester, object.getLstPriceListLines());
	}

	@Test
	public void createPriceListLineTC3() {
		PriceListLine line2 = object.createPriceListLine(120, product);
		PriceListLine line3 = new PriceListLine(23, product);
		object.addPriceListLine(line3);

		ArrayList<PriceListLine> tester = new ArrayList<PriceListLine>();
		tester.add(line);
		tester.add(line2);
		tester.add(line3);
		assertEquals(tester, object.getLstPriceListLines());
	}

	@Test
	public void createPriceListLineTC4() {
		PriceListLine line2 = object.createPriceListLine(120, product);
		PriceListLine line3 = new PriceListLine(23, product);
		object.addPriceListLine(line3);
		object.removePriceListLine(line3);

		ArrayList<PriceListLine> tester = new ArrayList<PriceListLine>();
		tester.add(line);
		tester.add(line2);
		assertEquals(tester, object.getLstPriceListLines());
	}

}
