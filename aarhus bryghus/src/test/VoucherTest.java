package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import application.model.products.Voucher;

public class VoucherTest {
	Voucher object;

	@Before
	public void setUp() throws Exception {
		object = new Voucher("Klippekort", "Klippekort, der kan bruges til fadøl", 10, 4);
	}

	@Test
	public void TC1Constructor() {
		String name = object.getName();
		String description = object.getDescription();
		int inStock = object.getInStock();
		int stamp = object.getStamp();
		assertEquals("Klippekort",name);
		assertEquals("Klippekort, der kan bruges til fadøl",description);
		assertEquals(10,inStock);
		assertEquals(4,stamp);
	}

	@Test
	public void toStringTC2() {
		String expected = "Klippekort : 4 klip";
		String actual = object.toString();
		assertEquals(expected,actual);
	}
}

