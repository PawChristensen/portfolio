package test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import application.model.Order;
import application.model.OrderLine;
import application.model.PriceList;
import application.model.PriceListLine;
import application.model.Product;

public class ProductTest {

	private Product product;
	private OrderLine orderLine;
	private OrderLine orderLine2;
	private PriceList butik;
	private PriceListLine pll;
	private Order order;
	
	@Before
	public void setUp() throws Exception {
		product = new Product("Krus","Plastik krus til en god pris.",10);
		
		butik = new PriceList("Butik");
		pll = butik.createPriceListLine(100, product);
		Order o1 = new Order(LocalDateTime.of(2017, 1, 1, 13, 30), butik);
		
		
		orderLine = o1.createOrderLine(pll.getProduct(), 10);
		orderLine2 = o1.createOrderLine(pll.getProduct(), 20);
	}

	@Test
	public void TC1Constructor() {
		String name = product.getName();
		String description = product.getDescription();
		int inStock = product.getInStock();
		
		assertEquals("Krus", name);
		assertEquals("Plastik krus til en god pris.", description);
		assertEquals(10, inStock);	
	}
	
	@Test
	public void getLstOrderLinesTC2() {	
		ArrayList<OrderLine> expected = new ArrayList<>();
		expected.add(orderLine);
		expected.add(orderLine2);
		
		assertEquals(expected, product.getLstOrderLines());		
	}
	
	@Test
	public void removeOrderLinesTC3() {	
		ArrayList<OrderLine> expected = new ArrayList<>();
		expected.add(orderLine);

		product.removeOrderLines(orderLine2);
		
		assertEquals(expected, product.getLstOrderLines());		
	}
	
	@Test
	public void getLstPriceListLinesTC4() {	
		PriceListLine pll2 = butik.createPriceListLine(20, product);
		
		ArrayList<PriceListLine> expected = new ArrayList<>();
		expected.add(pll);		
		expected.add(pll2);
				
		assertEquals(expected, product.getLstPriceListLines());		
	}
	
	@Test
	public void removeOrderLinesTC5() {	
		
		ArrayList<OrderLine> expected = new ArrayList<>();
		expected.add(orderLine2);		
				
		product.removeOrderLines(orderLine);
		
		assertEquals(expected, product.getLstOrderLines());		
	}
	
	@Test
	public void removePriceListLineTC6() {	
		PriceListLine pll2 = butik.createPriceListLine(20, product);
		
		ArrayList<PriceListLine> expected = new ArrayList<>();
		expected.add(pll);		
				
		product.removePriceListLine(pll2);
		
		assertEquals(expected, product.getLstPriceListLines());		
	}
	
	@Test
	public void setNameTC7() {	
		product.setName("Store Krus");

		String expected = "Store Krus";	
		String actual = product.getName();

		assertEquals(expected, actual);		
	}
	
	@Test
	public void setDescriptionTC8() {	
		product.setDescription("Billige krus");

		String expected = "Billige krus";	
		String actual = product.getDescription();

		assertEquals(expected, actual);		
	}
	
	@Test
	public void setInStockTC9() {	
		product.setInStock(22);

		int expected = 22;	
		int actual = product.getInStock();

		assertEquals(expected, actual);		
	}
	
	@Test
	
	public void addAmountToStockTC10() {				
		try {
			product.addAmountToStock(-15);
			fail();
		} catch (RuntimeException e) {
			assertEquals("Kan ikke ændre antallet, da total på lager på " + product.getName() + " bliver negativt",e.getMessage());
		}		
	}
	
	@Test
	public void addAmountToStockTC11() {	
		product.addAmountToStock(10);
		
		int expected = 20;	
		int actual = product.getInStock();
		assertEquals(expected, actual);		
	}
	
	@Test
	
	public void removeAmountFromStockTC12() {				
		try {
			product.removeAmountFromStock(15);
			fail();
		} catch (RuntimeException e) {
			assertEquals("Kan ikke ændre antallet, da total på lager bliver negativt", e.getMessage());
		}		
	}
	
	@Test
	public void removeAmountFromStockTC13() {	
		product.removeAmountFromStock(10);
		
		int expected = 0;	
		int actual = product.getInStock();
		assertEquals(expected, actual);		
	}
	
	
	@Test
	public void getSoldTC14() {	
		int actual = product.getSold(butik);
		
		int expected = 30;	

		assertEquals(expected, actual);		
	}
	
	
	
	
	
}
