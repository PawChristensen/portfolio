package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import application.model.products.Keg;

public class KegTest {
	Keg object;

	@Before
	public void setUp() throws Exception {
		object = new Keg("Klosterbryg", "En dejlig øl", 10, 20);
	}

	@Test
	public void TC1Constructor() {
		String name = object.getName();
		String description = object.getDescription();
		int inStock = object.getInStock();
		double liter = object.getLiter();
		assertEquals("Klosterbryg", name);
		assertEquals("En dejlig øl", description);
		assertEquals(10, inStock);
		assertEquals(20, liter, 0.0001);
	}

	@Test
	public void toStringTC2() {
		String expected = "Klosterbryg Fustage, 20.0L";
		String actual = object.toString();
		assertEquals(expected, actual);
	}
}
