package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import application.model.products.Beer;
import application.model.products.Delivery;

public class DeliveryTest {

	private Delivery object;
	
	@Before
	public void setUp() throws Exception {
		object = new Delivery("Levering", "indenfor 20km", 100);
	}

	@Test
	public void TC1Constructor() {
		String name = object.getName();
		String description = object.getDescription();
		int inStock = object.getInStock();


		assertEquals("Levering", name);
		assertEquals("indenfor 20km", description);
		assertEquals(100, inStock);
	}
	
}
