package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import application.model.products.DraftBeerMachine;

public class DraftBeerMachineTest {
	DraftBeerMachine object;

	@Before
	public void setUp() throws Exception {
		object = new DraftBeerMachine("2-haners fadølsanlæg", "Kan udlejes", 10);
	}

	@Test
	public void TC1Constructor() {
		String name = object.getName();
		String description = object.getDescription();
		int inStock = object.getInStock();

		assertEquals("2-haners fadølsanlæg", name);
		assertEquals("Kan udlejes", description);
		assertEquals(10, inStock);
	}

	@Test
	public void toStringTC2() {
		String expected = "2-haners fadølsanlæg";
		String actual = object.toString();
		assertEquals(expected, actual);
	}

}
