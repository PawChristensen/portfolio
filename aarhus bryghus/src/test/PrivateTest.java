package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import application.model.Private;

public class PrivateTest {
	Private object;


	@Before
	public void setUp() throws Exception {
		object = new Private("Hans Henrik", "Søndervænget 20", 8000, 70121416, "hansH@gmail.com");
	}

	@Test
	public void TC1Constructor() {
		String navn = object.getName();
		String address = object.getAddress();
		int zipCode = object.getZipCode();
		int phone = object.getPhone();
		String mail = object.getMail();

		assertEquals("Hans Henrik",navn);
		assertEquals("Søndervænget 20",address);
		assertEquals(8000,zipCode);
		assertEquals(70121416,phone);
		assertEquals("hansH@gmail.com",mail);
	}


}
