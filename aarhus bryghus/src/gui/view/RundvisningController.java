package gui.view;

import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Random;

import application.model.Customer;
import application.model.TourBooking;
import application.service.Service;
import gui.MainApp;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class RundvisningController {
	private static Pane pane;
	private static Stage stage;

	@FXML
	private TableView<TourBooking> tblRundvisninger;

	@FXML
	private TableColumn<TourBooking, LocalDate> columnDato;

	@FXML
	private TableColumn<TourBooking, LocalTime> columnTid;

	@FXML
	private TableColumn<TourBooking, Integer> columnAntal;

	@FXML
	private TableColumn<TourBooking, Customer> columnKunde;

	@FXML
	private TableColumn<TourBooking, Integer> columnTelefon;

	public static void initWindow() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/RundvisningView.fxml"));
			pane = (Pane) loader.load();
			stage = new Stage();
			stage.setTitle("Kasseapparatet - Nem-Bryg");
			stage.setScene(new Scene(pane));
			stage.setResizable(false);
			stage.getIcons().add(new Image("/resources/images/icon.png"));
			stage.show();
		} catch (IOException e) {
			System.out.println(e.getMessage());

		}
	}

	@FXML
	private ScrollPane scrollPane;

	@FXML
	private Line l1;

	@FXML
	private Line l2;

	@FXML
	private Line l3;

	@FXML
	private Line l4;

	@FXML
	private Line l5;

	@FXML
	private Line l6;

	@FXML
	private Line l7;

	@FXML
	private Line l8;

	@FXML
	private Line l9;

	@FXML
	private Line l10;

	@FXML
	private Line l11;

	@FXML
	private Line l12;

	@FXML
	private Line l13;

	@FXML
	private Line l14;

	@FXML
	private Line l15;

	@FXML
	private Line l16;

	@FXML
	private Line l17;

	@FXML
	private Line l18;

	@FXML
	private Line l19;

	@FXML
	private Line l20;

	@FXML
	private Line l21;

	@FXML
	private Line l22;

	@FXML
	private Line l23;

	@FXML
	private Line l24;

	@FXML
	private Text mondayText;

	@FXML
	private Text wednesdayText;

	@FXML
	private Text thursdayText;

	@FXML
	private Text fridayText;

	@FXML
	private Text saturdayText;

	@FXML
	private Text sundayText;

	@FXML
	private Text tuesdayText;

	@FXML
	private AnchorPane mainPane;

	@FXML
	private Text ugeText;

	@FXML
	private DatePicker weekPicker;

	@FXML
	void nextWeekAction() {
		weekPicker.setValue(current.plusDays(7));
	}

	@FXML
	void pastWeekAction() {
		weekPicker.setValue(current.minusDays(7));
	}

	@FXML
	void selectedWeekAction() {
		if (weekPicker.getValue() != null) {
			current = weekPicker.getValue();
		} else {
			weekPicker.setValue(LocalDate.now());
			current = weekPicker.getValue();
		}
		setWeeks();
		updateTourBookings();
	}

	private LocalDate monday, tuesday, wednesday, thursday, friday, saturday, sunday, current;

	@FXML
	public void initialize() {
		scrollPane.setVvalue(0.5);
		weekPicker.setValue(LocalDate.now());
		current = weekPicker.getValue();
		setWeeks();
		updateTourBookings();
	}

	public void updateTourBookings() {
		ArrayList<Node> tempNodesToBeRemoved = new ArrayList<Node>();
		for (Node n : mainPane.getChildren()) {
			if (n instanceof Label || n instanceof Rectangle) {
				tempNodesToBeRemoved.add(n);
			}
		}
		mainPane.getChildren().removeAll(tempNodesToBeRemoved);

		for (TourBooking tour : Service.getService().getTourBookings()) {
			if (tour.getDate().toLocalDate().isAfter(monday.minusDays(1))
					&& tour.getDate().toLocalDate().isBefore(sunday.plusDays(1))) {
				Random random = new Random();
				int red = random.nextInt(255);
				int green = random.nextInt(255);
				int blue = random.nextInt(255);
				int startPosY = (tour.getDate().toLocalTime().getHour() * 60)
						+ tour.getDate().toLocalTime().getMinute();
				int startPosX = 0;
				if (tour.getDate().toLocalDate().equals(monday)) {
					startPosX = 59;
				} else if (tour.getDate().toLocalDate().equals(tuesday)) {
					startPosX = 197;
				} else if (tour.getDate().toLocalDate().equals(wednesday)) {
					startPosX = 335;
				} else if (tour.getDate().toLocalDate().equals(thursday)) {
					startPosX = 473;
				} else if (tour.getDate().toLocalDate().equals(friday)) {
					startPosX = 611;
				} else if (tour.getDate().toLocalDate().equals(saturday)) {
					startPosX = 749;
				} else {
					startPosX = 887;
				}
				Rectangle rec = new Rectangle(startPosX, startPosY, 138, 120);
				String startTid = "";
				if (tour.getDate().toLocalTime().getMinute() >= 10) {
					startTid = "Starttid: " + tour.getDate().toLocalTime().getHour() + ":"
							+ tour.getDate().toLocalTime().getMinute();
				} else {
					startTid = "Starttid: " + tour.getDate().toLocalTime().getHour() + ":0"
							+ tour.getDate().toLocalTime().getMinute();
				}
				Label text = new Label(startTid + "\n" + tour.getOrderLine().getOrder().getCustomer().getName() + "\n"
						+ tour.getOrderLine().getOrder().getCustomer().getPhone() + "\n"
						+ tour.getOrderLine().getOrder().getCustomer().getMail() + "\n" + "Antal: "
						+ tour.getOrderLine().getQuantity());
				text.setLayoutX(startPosX + 5);
				text.setLayoutY(startPosY + 7);
				text.setTextFill(Color.WHITE);
				rec.setStroke(Color.BLACK);
				rec.setFill(Color.rgb(red, green, blue));

				mainPane.getChildren().add(rec);
				mainPane.getChildren().add(text);
			}
		}
	}

	public void setWeeks() {
		if (current.getDayOfWeek().equals(DayOfWeek.MONDAY)) {
			mondayText.setText("Man " + current);
			tuesdayText.setText("Tirs " + current.plusDays(1));
			wednesdayText.setText("Ons " + current.plusDays(2));
			thursdayText.setText("Tors " + current.plusDays(3));
			fridayText.setText("Fre " + current.plusDays(4));
			saturdayText.setText("Lør " + current.plusDays(5));
			sundayText.setText("Søn " + current.plusDays(6));
			monday = current;
			tuesday = current.plusDays(1);
			wednesday = current.plusDays(2);
			thursday = current.plusDays(3);
			friday = current.plusDays(4);
			saturday = current.plusDays(5);
			sunday = current.plusDays(6);
		} else if (current.getDayOfWeek().equals(DayOfWeek.TUESDAY)) {
			mondayText.setText("Man " + current.minusDays(1));
			tuesdayText.setText("Tirs " + current);
			wednesdayText.setText("Ons " + current.plusDays(1));
			thursdayText.setText("Tors " + current.plusDays(2));
			fridayText.setText("Fre " + current.plusDays(3));
			saturdayText.setText("Lør " + current.plusDays(4));
			sundayText.setText("Søn " + current.plusDays(5));
			monday = current.minusDays(1);
			tuesday = current;
			wednesday = current.plusDays(1);
			thursday = current.plusDays(2);
			friday = current.plusDays(3);
			saturday = current.plusDays(4);
			sunday = current.plusDays(5);
		} else if (current.getDayOfWeek().equals(DayOfWeek.WEDNESDAY)) {
			mondayText.setText("Man " + current.minusDays(2));
			tuesdayText.setText("Tirs " + current.minusDays(1));
			wednesdayText.setText("Ons " + current);
			thursdayText.setText("Tors " + current.plusDays(1));
			fridayText.setText("Fre " + current.plusDays(2));
			saturdayText.setText("Lør " + current.plusDays(3));
			sundayText.setText("Søn " + current.plusDays(4));
			monday = current.minusDays(2);
			tuesday = current.minusDays(1);
			wednesday = current;
			thursday = current.plusDays(1);
			friday = current.plusDays(2);
			saturday = current.plusDays(3);
			sunday = current.plusDays(4);
		} else if (current.getDayOfWeek().equals(DayOfWeek.THURSDAY)) {
			mondayText.setText("Man " + current.minusDays(3));
			tuesdayText.setText("Tirs " + current.minusDays(2));
			wednesdayText.setText("Ons " + current.minusDays(1));
			thursdayText.setText("Tors " + current);
			fridayText.setText("Fre " + current.plusDays(1));
			saturdayText.setText("Lør " + current.plusDays(2));
			sundayText.setText("Søn " + current.plusDays(3));
			monday = current.minusDays(3);
			tuesday = current.minusDays(2);
			wednesday = current.minusDays(1);
			thursday = current;
			friday = current.plusDays(1);
			saturday = current.plusDays(2);
			sunday = current.plusDays(3);
		} else if (current.getDayOfWeek().equals(DayOfWeek.FRIDAY)) {
			mondayText.setText("Man " + current.minusDays(4));
			tuesdayText.setText("Tirs " + current.minusDays(3));
			wednesdayText.setText("Ons " + current.minusDays(2));
			thursdayText.setText("Tors " + current.minusDays(1));
			fridayText.setText("Fre " + current);
			saturdayText.setText("Lør " + current.plusDays(1));
			sundayText.setText("Søn " + current.plusDays(2));
			monday = current.minusDays(4);
			tuesday = current.minusDays(3);
			wednesday = current.minusDays(2);
			thursday = current.minusDays(1);
			friday = current;
			saturday = current.plusDays(1);
			sunday = current.plusDays(2);
		} else if (current.getDayOfWeek().equals(DayOfWeek.SATURDAY)) {
			mondayText.setText("Man " + current.minusDays(5));
			tuesdayText.setText("Tirs " + current.minusDays(4));
			wednesdayText.setText("Ons " + current.minusDays(3));
			thursdayText.setText("Tors " + current.minusDays(2));
			fridayText.setText("Fre " + current.minusDays(1));
			saturdayText.setText("Lør " + current);
			sundayText.setText("Søn " + current.plusDays(1));
			monday = current.minusDays(5);
			tuesday = current.minusDays(4);
			wednesday = current.minusDays(3);
			thursday = current.minusDays(2);
			friday = current.minusDays(1);
			saturday = current;
			sunday = current.plusDays(1);
		} else {
			mondayText.setText("Man " + current.minusDays(6));
			tuesdayText.setText("Tirs " + current.minusDays(5));
			wednesdayText.setText("Ons " + current.minusDays(4));
			thursdayText.setText("Tors " + current.minusDays(3));
			fridayText.setText("Fre " + current.minusDays(2));
			saturdayText.setText("Lør " + current.minusDays(1));
			sundayText.setText("Søn " + current);
			monday = current.minusDays(6);
			tuesday = current.minusDays(5);
			wednesday = current.minusDays(4);
			thursday = current.minusDays(3);
			friday = current.minusDays(2);
			saturday = current.minusDays(1);
			sunday = current;
		}
		int weekNumber = (sunday.getDayOfYear() / 7);
		ugeText.setText("Uge: \n" + weekNumber);
	}
}
