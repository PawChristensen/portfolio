package gui.view;

import java.io.File;

import application.service.Service;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class MainViewController {

	@FXML
	private Button btnSalg;

	@FXML
	private Button btnLagerstyring;

	@FXML
	private Button btnStatistik;

	@FXML
	private Button btnRundvisninger;

	@FXML
	private ImageView imgLogo;
	
	@FXML
	private void btnSalgAction() {
		PrislisteValgController.initWindow();
	}

	@FXML
	private void btnLagerStyringAction() {
		LagerStyringController.initWindow();
	}

	@FXML
	private void btnStatistikAction() {
		StatistikController.initWindow();
	}

	@FXML
	private void btnUdlejedeProdukterAction() {
		UdlejedeProdukterController.initWindow();
	}

	@FXML
	private void btnOrdreOversigtAction() {
		OrdreOversigtController.initWindow();
	}

	@FXML
	private void btnRundvisningerAction() {
		RundvisningController.initWindow();
	}

	@FXML
	void initialize() {

	    Image image = new Image("/resources/images/nem.png");
		imgLogo.setImage(image);
		Service.getService().initContent();
	}

}
