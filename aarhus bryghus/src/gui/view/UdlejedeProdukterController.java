package gui.view;

import java.io.IOException;
import java.time.LocalDate;

import application.model.Order;
import application.model.OrderLine;
import application.model.PriceListLine;
import application.model.Product;
import application.model.Reservation;
import application.model.products.DraftBeerMachine;
import application.model.products.Tour;
import application.service.Service;
import gui.MainApp;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class UdlejedeProdukterController {

	private static Pane pane;
	private static Stage stage;
	
	
	  @FXML
	    private TableView<OrderLine> tblUdlejedeProdukter;

	    @FXML
	    private TableColumn<OrderLine, String> columnNavn;

	    @FXML
	    private TableColumn<OrderLine, Integer> columnAntal;
	    
	    @FXML
	    private TableColumn<OrderLine, LocalDate> columnFra;

	    @FXML
	    private TableColumn<OrderLine, LocalDate> columnTil;
	
	    @FXML
	    private TableColumn<OrderLine, Integer> columnOrdreNr;
	    
	    @FXML
	    private Button btnAabenOrdre;
	    
	    
public static void initWindow() {
		
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/UdlejedeProdukter.fxml"));
			pane = (Pane) loader.load();
			stage = new Stage();
			stage.setTitle("Nem-Bryg");
			stage.setScene(new Scene(pane));
			stage.getIcons().add(new Image("/resources/images/icon.png"));
			stage.show();
			
		}
		catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	 @FXML
	 void initialize() {
		 
		 /* Udlejede produkter tabellen */
		 columnNavn.setCellValueFactory(new PropertyValueFactory<OrderLine, String>("ProductName"));
		 columnAntal.setCellValueFactory(new PropertyValueFactory<OrderLine, Integer>("quantity"));	
		 columnFra.setCellValueFactory(new PropertyValueFactory<OrderLine, LocalDate>("FromDate"));
		 columnTil.setCellValueFactory(new PropertyValueFactory<OrderLine, LocalDate>("ToDate"));	
		 columnOrdreNr.setCellValueFactory(new PropertyValueFactory<OrderLine, Integer>("OrderNumber"));
	
		 tblUdlejedeProdukter.getItems().setAll(Service.getService().getLeasedProducts());
		 	 
			 }
	 
	 @FXML
	 void openOrder() {
		 
		 if (tblUdlejedeProdukter.getSelectionModel().getSelectedItem() != null) {
			
			 OrderLine ol = tblUdlejedeProdukter.getSelectionModel().getSelectedItem();
			 
			 SalgController.initWindow(ol.getOrder().getPriceList(),ol.getOrder());
			 
		 }
		 
		 
	
	 }
	 
}
