package gui.view;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import application.model.Order;
import application.model.OrderLine;
import application.model.Payment;
import application.model.PaymentMethod;
import application.model.PriceList;
import application.model.PriceListLine;
import application.model.Product;
import application.model.products.Beer;
import application.model.products.GiftBasket;
import application.model.products.Voucher;
import application.service.Service;
import gui.MainApp;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.Chart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.PieChart.Data;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class StatistikController {
	private static Pane pane;
	private static Stage stage;

	public static void initWindow() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/StatistikView.fxml"));
			pane = (Pane) loader.load();
			stage = new Stage();
			stage.setTitle("Kasseapparatet - Nem-Bryg");
			stage.setScene(new Scene(pane));
			stage.setResizable(false);
			stage.getIcons().add(new Image("/resources/images/icon.png"));
			stage.show();
		} catch (IOException e) {
			System.out.println(e.getMessage());

		}
	}

	// --------------------- Tab 2: Øl statistik
	// --------------------------------------------

	@FXML
	private ComboBox<PriceList> priceListPickerBeer;

	@FXML
	private ComboBox<String> diagramPickerBeer;

	@FXML
	private AreaChart<String, Integer> areaChartBeer;

	@FXML
	private PieChart pieChartBeer;

	@FXML
	private CheckBox chkCheckAllBeer;

	@FXML
	private TableView<Product> tblVBeers;

	@FXML
	private TableColumn<Product, String> tblCbeersName;

	@FXML
	private TableColumn<Product, Integer> tblCbeersSold;

	@FXML
	private Label lblPercentageBeer;

	private PriceList selectedPriceListBeer;
	private ArrayList<String> lstChartNamesBeer;
	private ArrayList<Chart> lstChartsBeer;

	public void updatePieChartBeer() {
		ObservableList<PieChart.Data> lstData = FXCollections.observableArrayList();
		for (Product p : tblVBeers.getSelectionModel().getSelectedItems()) {
			int count = tblCbeersSold.getCellData(p);
			Beer save = (Beer) p;
			lstData.add(new Data(save.getToString(), count));
		}
		pieChartBeer.setData(lstData);
		for (final PieChart.Data d : pieChartBeer.getData()) {
			double dynAllData = 0;
			for (final PieChart.Data data : pieChartBeer.getData()) {
				dynAllData += data.getPieValue();
			}
			final double finalAllData = dynAllData;
			d.getNode().addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					lblPercentageBeer.setText("Procentsats: " + (int) ((d.getPieValue() / finalAllData) * 100) + "%");
					lblPercentageBeer.setVisible(true);
				}
			});
		}
	}

	public void updateBeerList() {
		ObservableList<Product> lstProducts = FXCollections.observableArrayList();

		for (Product p : Service.getService().getProducts()) {
			for (OrderLine ol : p.getLstOrderLines()) {
				for (PriceListLine pl : p.getLstPriceListLines()) {
					if (selectedPriceListBeer.getLstPriceListLines().contains(pl) && p instanceof Beer
							&& (ol.getOrder().getPriceList().equals(selectedPriceListBeer)
									|| selectedPriceListBeer.getTitle() == "Alle lister")) {
						if (!lstProducts.contains(p)) {
							lstProducts.add(p);
						}
					}
				}
			}
		}

		for (Product p : Service.getService().getProducts()) {
			if (p instanceof GiftBasket) {
				for (Beer b : ((GiftBasket) p).getLstBeers()) {
					for (PriceListLine pl : p.getLstPriceListLines()) {
						for (OrderLine ol : p.getLstOrderLines()) {
							if (selectedPriceListBeer.getLstPriceListLines().contains(pl) && b instanceof Beer
									&& (ol.getOrder().getPriceList().equals(selectedPriceListBeer)
											|| selectedPriceListBeer.getTitle() == "Alle lister")) {
								if (!lstProducts.contains(b)) {
									lstProducts.add(b);
								}
							}
						}
					}
				}
			}
		}

		tblVBeers.getItems().setAll(lstProducts);
	}

	@FXML
	void chooseDiagramBeer() {
		for (Chart c : lstChartsBeer) {
			if (diagramPickerBeer.getSelectionModel().getSelectedItem().equals(c.getTitle())) {
				c.setVisible(true);
			} else {
				if (!diagramPickerBeer.getSelectionModel().getSelectedItem().equals("Cirkel diagram")) {
					lblPercentageBeer.setVisible(false);
				}
				c.setVisible(false);
			}
		}
	}

	@FXML
	void choosePriceListBeer() {
		selectedPriceListBeer = priceListPickerBeer.getSelectionModel().getSelectedItem();
		updateBeerList();
		chkCheckAllBeer.setSelected(false);
		checkAllBeer();
	}

	@FXML
	void checkAllBeer() {
		if (!chkCheckAllBeer.isSelected()) {
			tblVBeers.getSelectionModel().clearSelection();
		} else {
			for (Product p : tblVBeers.getItems()) {
				tblVBeers.getSelectionModel().select(p);
			}
		}
		lblPercentageBeer.setVisible(false);
		updatePieChartBeer();

	}

	@FXML
	void checkOneBeer() {
		chkCheckAllBeer.setSelected(false);
		updatePieChartBeer();
	}

	// --------------------------- Tab 1: Dagens salg
	// ---------------------------------------

	@FXML
	private TableView<Order> tblVOrders;

	@FXML
	private TableColumn<Order, Integer> tblCorderName;

	@FXML
	private TableColumn<Order, LocalDateTime> tblCorderDate;

	@FXML
	private TableColumn<Order, Double> tblCorderPrice;

	@FXML
	private CheckBox chkCheckAllOrder;

	@FXML
	private DatePicker fromOrder;

	@FXML
	private DatePicker toOrder;

	@FXML
	private StackedBarChart<String, Double> barChart;

	@FXML
	private ComboBox<PriceList> priceListPickerOrder;

	@FXML
	private ComboBox<String> diagramPickerOrder;

	@FXML
	private BarChart<String, Double> simpleBarChart;

	private PriceList selectedPriceListOrder;
	private ArrayList<String> lstChartNamesOrder;
	private ArrayList<Chart> lstChartsOrder;
	private LocalDate selectedFromDateOrder;
	private LocalDate selectedToDateOrder;

	@FXML
	void choosePriceListOrder() {
		selectedPriceListOrder = priceListPickerOrder.getValue();
		updateOrderList();
		chkCheckAllOrder.setSelected(false);
		checkAllOrder();
	}

	@FXML
	void chooseDiagramOrder() {
		for (Chart c : lstChartsOrder) {
			if (diagramPickerOrder.getSelectionModel().getSelectedItem().equals(c.getTitle())) {
				c.setVisible(true);
			} else {
				c.setVisible(false);
			}
		}
	}

	@FXML
	void checkOneOrder() {
		chkCheckAllOrder.setSelected(false);
		updateAdvancedBarChart();
		updateSimpleBarChart();
	}

	@FXML
	void checkAllOrder() {
		if (!chkCheckAllOrder.isSelected()) {
			tblVOrders.getSelectionModel().clearSelection();
		} else {
			for (Order o : tblVOrders.getItems()) {
				tblVOrders.getSelectionModel().select(o);
			}
		}
		updateAdvancedBarChart();
		updateSimpleBarChart();
	}

	@FXML
	void setFromDateOrder() {
		if (fromOrder.getValue() != null) {
			selectedFromDateOrder = fromOrder.getValue();
		} else {
			fromOrder.setValue(LocalDate.now());
			selectedFromDateOrder = fromOrder.getValue();
		}
		updateOrderList();
		chkCheckAllOrder.setSelected(false);
		checkAllOrder();
	}

	@FXML
	void setToDateOrder() {
		if (toOrder.getValue() != null) {
			selectedToDateOrder = toOrder.getValue();
		} else {
			toOrder.setValue(LocalDate.now().plusDays(1));
			selectedToDateOrder = toOrder.getValue();
		}
		updateOrderList();
		chkCheckAllOrder.setSelected(false);
		checkAllOrder();
	}

	public void updateOrderList() {
		ObservableList<Order> lstOrders = FXCollections.observableArrayList();
		for (Order o : Service.getService().getOrders()) {
			if (o.getLstOrderlines().size() != 0) {
				for (PriceListLine pl : o.getLstOrderlines().get(0).getProduct().getLstPriceListLines()) {
					if (o.getDate().toLocalDate().isBefore(selectedToDateOrder)
							&& o.getDate().toLocalDate().isAfter(selectedFromDateOrder.minusDays(1))
							&& selectedPriceListOrder.getLstPriceListLines().contains(pl)
							&& (o.getPriceList().equals(selectedPriceListOrder)
									|| selectedPriceListOrder.getTitle() == "Alle lister")) {
						if (!lstOrders.contains(o)) {
							lstOrders.add(o);
						}
					}
				}
			}
		}
		tblVOrders.getItems().setAll(lstOrders);
	}

	public void updateAdvancedBarChart() {
		ObservableList<Series<String, Double>> l1 = FXCollections.observableArrayList();
		Series<String, Double> s;
		for (Order o : tblVOrders.getSelectionModel().getSelectedItems()) {
			s = new Series<String, Double>();
			for (OrderLine ol : o.getLstOrderlines()) {
				String nameData = ol.getProduct().getName();
				if (ol.getProduct() instanceof Beer) {
					Beer save = (Beer) ol.getProduct();
					nameData = save.getToString();
				}
				s.getData().add(new javafx.scene.chart.XYChart.Data<String, Double>(nameData, ol.getTotalPrice()));
			}
			l1.add(s);
		}
		barChart.setData(l1);
	}

	public void updateSimpleBarChart() {
		ObservableList<Series<String, Double>> l2 = FXCollections.observableArrayList();
		Series<String, Double> s;
		for (Order o : tblVOrders.getSelectionModel().getSelectedItems()) {
			s = new Series<String, Double>();
			for (OrderLine ol : o.getLstOrderlines()) {
				String nameData = ol.getProduct().getName();
				if (ol.getProduct() instanceof Beer) {
					Beer save = (Beer) ol.getProduct();
					nameData = save.getToString();
				}
				s.getData().add(new javafx.scene.chart.XYChart.Data<String, Double>(nameData, ol.getTotalPrice()));
			}
			l2.add(s);
		}
		simpleBarChart.setData(l2);
	}

	// ------------------Tab 3: Klippekort statistik

	@FXML
	private ComboBox<String> diagramPickerVoucher;

	@FXML
	private ComboBox<PriceList> priceListPickerVoucher;

	@FXML
	private DatePicker fromVoucher;

	@FXML
	private DatePicker toVoucher;

	@FXML
	private PieChart pieChartVoucher;

	@FXML
	private CheckBox chkCheckAllVoucher;

	@FXML
	private LineChart<String, Double> lineChartVoucher;

	@FXML
	private TableView<Order> tblVVouchers;

	@FXML
	private TableColumn<Order, Integer> tblCvoucherNumber;

	@FXML
	private TableColumn<Order, Integer> tblCvoucherStamps;

	@FXML
	private TableColumn<Order, Integer> tblCvoucherUsed;

	@FXML
	private Label lblPercentageVoucher;

	private PriceList selectedPriceListVoucher;
	private ArrayList<String> lstChartNamesVoucher;
	private ArrayList<Chart> lstChartsVoucher;
	private LocalDate selectedFromDateVoucher;
	private LocalDate selectedToDateVoucher;

	@FXML
	void checkAllVoucher() {
		if (!chkCheckAllVoucher.isSelected()) {
			tblVVouchers.getSelectionModel().clearSelection();
		} else {
			for (Order o : tblVVouchers.getItems()) {
				tblVVouchers.getSelectionModel().select(o);
			}
		}
		lblPercentageVoucher.setVisible(false);
		updateLineChart();
		updatePieChartVoucher();
	}

	@FXML
	void checkOneVoucher() {
		chkCheckAllVoucher.setSelected(false);
		updateLineChart();
		updatePieChartVoucher();
	}

	@FXML
	void chooseDiagramVoucher() {
		for (Chart c : lstChartsVoucher) {
			if (diagramPickerVoucher.getSelectionModel().getSelectedItem().equals(c.getTitle())) {
				c.setVisible(true);
			} else {
				if (!diagramPickerVoucher.getSelectionModel().getSelectedItem().equals("Cirkel diagram")) {
					lblPercentageVoucher.setVisible(false);
				}
				c.setVisible(false);
			}
		}
	}

	@FXML
	void choosePriceListVoucher() {
		selectedPriceListVoucher = priceListPickerVoucher.getSelectionModel().getSelectedItem();
		updateVoucherList();
		chkCheckAllVoucher.setSelected(false);
		checkAllVoucher();
	}

	@FXML
	void setFromDateVoucher() {
		if (fromVoucher.getValue() != null) {
			selectedFromDateVoucher = fromVoucher.getValue();
		} else {
			fromVoucher.setValue(LocalDate.now());
			selectedFromDateVoucher = fromVoucher.getValue();
		}
		updateVoucherList();
		chkCheckAllVoucher.setSelected(false);
		checkAllVoucher();
	}

	@FXML
	void setToDateVoucher() {
		if (toVoucher.getValue() != null) {
			selectedToDateVoucher = toVoucher.getValue();
		} else {
			toVoucher.setValue(LocalDate.now().plusDays(1));
			selectedToDateVoucher = toOrder.getValue();
		}
		updateVoucherList();
		chkCheckAllVoucher.setSelected(false);
		checkAllVoucher();
	}

	public void updateVoucherList() {
		ObservableList<Order> lstOrders = FXCollections.observableArrayList();
		for (Order o : Service.getService().getOrders()) {
			for (OrderLine ol : o.getLstOrderlines()) {
				if (ol.getProduct() instanceof Voucher) {
					for (PriceListLine pl : ol.getProduct().getLstPriceListLines()) {
						if (o.getDate().toLocalDate().isBefore(selectedToDateVoucher)
								&& o.getDate().toLocalDate().isAfter(selectedFromDateVoucher.minusDays(1))
								&& selectedPriceListVoucher.getLstPriceListLines().contains(pl)
								&& (o.getPriceList().equals(selectedPriceListVoucher)
										|| selectedPriceListVoucher.getTitle() == "Alle lister")) {
							if (!lstOrders.contains(o)) {
								lstOrders.add(o);
							}
						}
					}
				}
			}
		}

		for (Order o : Service.getService().getOrders()) {
			for (OrderLine ol : o.getLstOrderlines()) {
				for (Payment pa : o.getLstPayments()) {
					if (pa.getPaymentMethod().equals(PaymentMethod.Klippekort)) {
						for (PriceListLine pl : ol.getProduct().getLstPriceListLines()) {
							if (o.getDate().toLocalDate().isBefore(selectedToDateVoucher)
									&& o.getDate().toLocalDate().isAfter(selectedFromDateVoucher.minusDays(1))
									&& selectedPriceListVoucher.getLstPriceListLines().contains(pl)
									&& (o.getPriceList().equals(selectedPriceListVoucher)
											|| selectedPriceListVoucher.getTitle() == "Alle lister")) {
								if (!lstOrders.contains(o)) {
									lstOrders.add(o);
								}
							}
						}
					}
				}
			}
		}
		tblVVouchers.getItems().setAll(lstOrders);
	}

	public void updateLineChart() {
		ObservableList<Series<String, Double>> l2 = FXCollections.observableArrayList();
		Series<String, Double> s;
		for (Order o : tblVVouchers.getSelectionModel().getSelectedItems()) {
			s = new Series<String, Double>();
			int count = 0;
			for (Payment p : o.getLstPayments()) {
				if (p.getPaymentMethod() == PaymentMethod.Klippekort) {
					count++;
					s.getData().add(new javafx.scene.chart.XYChart.Data<String, Double>(
							p.getDate().toLocalDate().toString(), (double) count));
				}
			}
			count = 0;
			l2.add(s);
		}
		lineChartVoucher.setData(l2);
	}

	public void updatePieChartVoucher() {
		ObservableList<PieChart.Data> lstData = FXCollections.observableArrayList();
		int sold = 0;
		int used = 0;
		for (Order o : tblVVouchers.getSelectionModel().getSelectedItems()) {
			for (OrderLine ol : o.getLstOrderlines()) {
				if (ol.getProduct() instanceof Voucher) {
					Voucher v = (Voucher) ol.getProduct();
					sold += (v.getStamp() * ol.getQuantity());
				}
			}
			used += o.getUsedStamps();
		}
		if (sold != 0) {
			lstData.add(new Data("Ikke brugt", sold - used));
			lstData.add(new Data("Brugt", used));
		}
		pieChartVoucher.setData(lstData);
		for (final PieChart.Data d : pieChartVoucher.getData()) {
			final double finalAllData = sold;
			d.getNode().addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					lblPercentageVoucher.setText("Procentsats: " + (int) ((d.getPieValue() / finalAllData) * 100)
							+ "%\n" + d.getName() + ": " + (int) d.getPieValue());
					lblPercentageVoucher.setVisible(true);
				}
			});
		}
	}

	// ----------------Metode der definerer hvad der sker når man åbner panet
	// -------

	@FXML
	void initialize() {
		// ------------------Tab 1: Dagens salg ----------------------

		priceListPickerOrder.getItems().setAll(Service.getService().getPriceLists());
		priceListPickerOrder.getItems().add(0, Service.getService().getAllPriceListsAsPriceList());
		priceListPickerOrder.getSelectionModel().select(0);
		selectedPriceListOrder = priceListPickerOrder.getSelectionModel().getSelectedItem();

		lstChartNamesOrder = new ArrayList<String>();
		lstChartNamesOrder.add(barChart.getTitle());
		lstChartNamesOrder.add(simpleBarChart.getTitle());
		diagramPickerOrder.getItems().setAll(lstChartNamesOrder);

		lstChartsOrder = new ArrayList<Chart>();
		lstChartsOrder.add(barChart);
		lstChartsOrder.add(simpleBarChart);

		barChart.getXAxis().setLabel("Produkter");
		barChart.getYAxis().setLabel("Samlet pris");
		simpleBarChart.getXAxis().setLabel("Produkter");
		simpleBarChart.getYAxis().setLabel("Pris");

		tblCorderName.setCellValueFactory(new PropertyValueFactory<Order, Integer>("orderNumber"));
		tblCorderDate.setCellValueFactory(new PropertyValueFactory<Order, LocalDateTime>("date"));
		tblCorderPrice.setCellValueFactory(new PropertyValueFactory<Order, Double>("totalPrice"));
		tblVOrders.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

		fromOrder.setValue(LocalDate.now());
		selectedFromDateOrder = fromOrder.getValue();
		toOrder.setValue(LocalDate.now().plusDays(1));
		selectedToDateOrder = toOrder.getValue();

		updateAdvancedBarChart();
		updateSimpleBarChart();
		updateOrderList();

		// ------------------Tab 2: Øl statistik ---------------------

		priceListPickerBeer.getItems().setAll(Service.getService().getPriceLists());
		priceListPickerBeer.getItems().add(0, Service.getService().getAllPriceListsAsPriceList());
		priceListPickerBeer.getSelectionModel().select(0);
		selectedPriceListBeer = priceListPickerBeer.getSelectionModel().getSelectedItem();

		lstChartNamesBeer = new ArrayList<String>();
		lstChartNamesBeer.add(pieChartBeer.getTitle());
		diagramPickerBeer.getItems().setAll(lstChartNamesBeer);

		lstChartsBeer = new ArrayList<Chart>();
		lstChartsBeer.add(pieChartBeer);

		areaChartBeer.getXAxis().setLabel("Dato");
		areaChartBeer.getYAxis().setLabel("Antal solgte vare");

		tblCbeersName.setCellValueFactory(new PropertyValueFactory<Product, String>("toString"));
		tblCbeersSold.setCellValueFactory(new Callback<CellDataFeatures<Product, Integer>, ObservableValue<Integer>>() {

			@Override
			public ObservableValue<Integer> call(CellDataFeatures<Product, Integer> param) {
				return new ReadOnlyObjectWrapper<Integer>(
						param.getValue().getSold(selectedPriceListBeer) + getGiftBasketBeers(param.getValue()));
			}

		});

		tblVBeers.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

		updatePieChartBeer();
		updateBeerList();

		// -------------------Tab 3: Klippekort statistik

		priceListPickerVoucher.getItems().setAll(Service.getService().getPriceLists());
		priceListPickerVoucher.getItems().add(0, Service.getService().getAllPriceListsAsPriceList());
		priceListPickerVoucher.getSelectionModel().select(0);
		selectedPriceListVoucher = priceListPickerVoucher.getSelectionModel().getSelectedItem();

		lstChartNamesVoucher = new ArrayList<String>();
		lstChartNamesVoucher.add(pieChartVoucher.getTitle());
		lstChartNamesVoucher.add(lineChartVoucher.getTitle());
		diagramPickerVoucher.getItems().setAll(lstChartNamesVoucher);

		lstChartsVoucher = new ArrayList<Chart>();
		lstChartsVoucher.add(pieChartVoucher);
		lstChartsVoucher.add(lineChartVoucher);

		lineChartVoucher.getXAxis().setLabel("Dato");
		lineChartVoucher.getYAxis().setLabel("Forbrugte klip");

		tblCvoucherNumber.setCellValueFactory(new PropertyValueFactory<Order, Integer>("orderNumber"));
		tblCvoucherStamps.setCellValueFactory(new PropertyValueFactory<Order, Integer>("stamps"));
		tblCvoucherUsed.setCellValueFactory(new PropertyValueFactory<Order, Integer>("usedStamps"));
		tblVVouchers.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

		fromVoucher.setValue(LocalDate.now());
		selectedFromDateVoucher = fromVoucher.getValue();
		toVoucher.setValue(LocalDate.now().plusDays(1));
		selectedToDateVoucher = toVoucher.getValue();

		updateVoucherList();
		updateLineChart();
		updatePieChartVoucher();
	}

	public int getGiftBasketBeers(Product p) {
		int result = 0;
		for (Order o : Service.getService().getOrders()) {
			for (OrderLine ol : o.getLstOrderlines()) {
				if (ol.getProduct() instanceof GiftBasket) {
					GiftBasket giftBasket = (GiftBasket) ol.getProduct();
					for (Beer b : giftBasket.getLstBeers()) {
						if (b.toString().equals(tblCbeersName.getCellData(p))) {
							result += ol.getQuantity();
						}
					}
				}
			}
		}
		return result;
	}
}
