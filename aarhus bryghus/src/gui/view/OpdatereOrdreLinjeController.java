package gui.view;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import application.model.Order;
import application.model.OrderLine;
import application.model.PriceListLine;
import application.model.Reservation;
import application.model.evaluators.IntegerEvaluator;
import application.model.evaluators.PriceEvaluator;
import application.model.products.DraftBeerMachine;
import application.service.Service;
import gui.MainApp;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class OpdatereOrdreLinjeController {

	
	private static Pane pane;
	private static Stage stage;
	private static OrderLine aktuelOrderLinje;
	private static SalgController parentController;
	
	    @FXML
	    private Label lblProduktNavn;
	    
	    @FXML
	    private Label lblError;

	    @FXML
	    private TextField txfAntal;

	    @FXML
	    private TextField txfStkPris;

	    @FXML
	    private TextField txfTotal;

	    @FXML
	    private Button btnGem;
	
	    @FXML
	    private HBox hbReservation;

	    @FXML
	    private DatePicker txfFromDate;

	    @FXML
	    private DatePicker txfToDate;

	    @FXML
	    private Label lblSecondOverskrift;
	    
	    @FXML
	    private HBox hbTourBooking;

	    @FXML
	    private DatePicker txfTourDate;

	    @FXML
	    private ComboBox<String> cbClock;
	    
	
	public static void initWindow(SalgController parent, OrderLine orderLine) {
	
		aktuelOrderLinje = orderLine;
		parentController = parent;

		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/OpdatereOrdreLinje.fxml"));
			pane = (Pane) loader.load();
			stage = new Stage();
			stage.setTitle("Nem-Bryg");
			stage.setScene(new Scene(pane));
			stage.getIcons().add(new Image("/resources/images/icon.png"));
			stage.show();
		}
		catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	 @FXML
	 void initialize() {
		 	
		 		lblProduktNavn.setText(aktuelOrderLinje.getProductName());
		 		txfAntal.setText(aktuelOrderLinje.getQuantity()+"");
		 		txfStkPris.setText(aktuelOrderLinje.getSinglePrice()+"");
		 		txfTotal.setEditable(false);
		 		hbReservation.setVisible(false);
		 		hbTourBooking.setVisible(false);
		 		btnGem.setDisable(false);

		 		 		
		 		if (aktuelOrderLinje.getReservation() != null) {
		 			Reservation aktuelReservation = aktuelOrderLinje.getReservation();
		 			
		 			hbReservation.setVisible(true);
		 			lblSecondOverskrift.setText("Reservation");
		 			txfFromDate.setValue(aktuelReservation.getFromDate());
		 			txfToDate.setValue(aktuelReservation.getToDate());	
		 		}
		 		else if (aktuelOrderLinje.getTourbooking() != null) {
					
		 			hbTourBooking.setVisible(true);
		 			lblSecondOverskrift.setText("Rundvisning");
		 			
		 			txfTourDate.setValue(aktuelOrderLinje.getTourbooking().getDate().toLocalDate());
		 			
		 			cbClock.getItems().add("10:00");
		 			cbClock.getItems().add("11:00");
		 			cbClock.getItems().add("12:00");
		 			cbClock.getItems().add("13:00");
		 			
		 			cbClock.getSelectionModel().select(aktuelOrderLinje.getTourbooking().getDate().toLocalTime()+"");	
				}
		 		
		 		updateTotal();
			    }
	 
	 
	 /*
	  * Udregner total prisen for det valgte antal
	  * */
	 @FXML
	 private void updateTotal() {
		 

		 
		 if (!Service.getService().isValidInteger(txfAntal.getText())) {
			lblError.setText("Indtast et gyldigt antal");
			btnGem.setDisable(true);
		 }
		 else if (!Service.getService().isValidDouble(txfStkPris.getText())) {
			 lblError.setText("Indtast en gyldig pris");
			 btnGem.setDisable(true);
		}
		 else {
		 try {
				double total = Double.parseDouble(txfAntal.getText())*Double.parseDouble(txfStkPris.getText()); 
				
				txfTotal.setText(total+"");
				if (!Service.getService().checkIfStockIsAvailable(aktuelOrderLinje.getProduct(), Integer.parseInt(txfAntal.getText()))) {											
					btnGem.setDisable(true);
					lblError.setText("Det ønskede antal er ikke på lager.");
				}
				else {
					lblError.setText("");
					btnGem.setDisable(false);
				}
				
				
} catch (Exception e) {
	txfTotal.setText("fejl");
	lblError.setText("Der er indtastet ugyldige værdier i felterne.");
	btnGem.setDisable(true);
}
					
	 }}
	
	 
	 /**
	  * Opdatere ordrelinje, med det angivne, antal samt pris
	  * */
	 @FXML
	 private void updateActualOrderLine() {
		 
		 //Hvis varen kræver en reservation sættes denne på her.
		 if (aktuelOrderLinje.getReservation() != null) {
			 
			 Reservation aktuelReservation = aktuelOrderLinje.getReservation();
			 
			 if (!Service.getService().getLeasedProducts().contains(aktuelOrderLinje)) {			
				 if (txfFromDate.getValue() == null || txfToDate.getValue() == null) {
						if (txfFromDate.getValue() == null) {
							txfFromDate.setValue(aktuelReservation.getFromDate());
						}
						if (txfToDate.getValue() == null) {
							txfToDate.setValue(aktuelReservation.getToDate());
						}
					}
				 else if (txfFromDate.getValue().isAfter(txfToDate.getValue()) || txfFromDate.getValue().isBefore(LocalDate.now())) {
					lblError.setText("Fra dato, skal være før til dato, og ikke før dd.");
			 }
			 else {
				
				 Service.getService().updateOrderLine(aktuelOrderLinje, Integer.parseInt(txfAntal.getText()), Double.parseDouble(txfTotal.getText()), aktuelReservation, txfFromDate.getValue(), txfToDate.getValue(), null, null);
				 
				 stage.close();
			 }
			 }
			 else {
				 Service.getService().updateOrderLine(aktuelOrderLinje, Integer.parseInt(txfAntal.getText()), Double.parseDouble(txfTotal.getText()), aktuelReservation, txfFromDate.getValue(), txfToDate.getValue(), null, null);
				 
				 stage.close();
			}
			 
		 	 
		 }
		 else if (aktuelOrderLinje.getTourbooking() != null) {
			 
			 if (txfTourDate.getValue() == null) {
				 txfTourDate.setValue(LocalDate.now());
			}
			 else if (!txfTourDate.getValue().isAfter(LocalDate.now()) && !txfTourDate.getValue().isEqual(LocalDate.now())) {
					lblError.setText("Ugyldig dato - må ikke være før dd.");
			 }
			 else {
				 Service.getService().updateOrderLine(aktuelOrderLinje, Integer.parseInt(txfAntal.getText()), Double.parseDouble(txfTotal.getText()), null, null, null, aktuelOrderLinje.getTourbooking(), txfTourDate.getValue().atTime(LocalTime.parse(cbClock.getValue())));			
				 
					stage.close();
			 }
						}
		 else {
			Service.getService().updateOrderLine(aktuelOrderLinje, Integer.parseInt(txfAntal.getText()), Double.parseDouble(txfTotal.getText()), null, null, null, null, null); 
				 
			stage.close();
		 }
		
		 /* opdatere tableview med ordrelinjer */
		 parentController.updateOrderRightSide();
	 }
	 
	 
}
