package application.model.evaluators;

public interface Evaluator {
	public boolean isValid(String s);
}
