package application.model.evaluators;

public class IntegerEvaluator implements Evaluator {

	@Override
	public boolean isValid(String s) {
		
	try {
		int i = Integer.parseInt(s);
		if (i <= 0) {
			return false;
		}
		return true;
	} catch (Exception e) {
		return false;
	}
	}
}
