package application.model.products;

import application.model.Product;

public class Beer extends Product {

	private double cl;
	private double alcohol;

	public Beer(String name, String desciption,int inStock, double cl,double alcohol) {
		super(name, desciption,inStock);
		this.cl = cl;
		this.alcohol = alcohol;
	}

	public double getCl() {
		return cl;
	}

	public void setCl(double cl) {
		this.cl = cl;
	}

	public double getAlcohol() {
		return alcohol;
	}

	public void setAlcohol(double alcohol) {
		this.alcohol = alcohol;
	}

	@Override
	public String toString() {
		if (cl == 40) {
			return super.getName() + " fadøl, " + cl + " cl, "+alcohol+"%";
		} else {
			return super.getName() + " flaske, " + cl + " cl, "+alcohol+"%";
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(alcohol);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(cl);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	// ---Method for tableView
	@Override
	public String getToString() {
		return toString();
	}

}
