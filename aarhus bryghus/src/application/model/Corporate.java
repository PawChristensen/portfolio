package application.model;

public class Corporate extends Customer {
	private int cvr;
	private String contactPerson;

	public Corporate(String name, String address,int zipCode, int phone, String mail, int cvr, String contactPerson) {
		super(name, address,zipCode, phone, mail);
		this.cvr = cvr;
		this.contactPerson = contactPerson;
	}

	public int getCvr() {
		return cvr;
	}

	public void setCvr(int cvr) {
		this.cvr = cvr;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	
	@Override
	public String toString() {
		return super.toString() + "CVR: " + this.cvr + "\n" + "Kontakt: " + this.contactPerson;	
	}



}
