package application.model;

import java.util.ArrayList;

public abstract class Customer {
	private String name;
	private String address;
	private int zipCode;
	private int phone;
	private String mail;
	private ArrayList<Order> lstOrders;

	public Customer(String name, String address,int zipCode, int phone, String mail) {
		this.name = name;
		this.address = address;
		this.zipCode = zipCode;
		this.phone = phone;
		this.mail = mail;
		this.lstOrders = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getZipCode() {
		return zipCode;
	}

	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public ArrayList<Order> getLstOrder() {
		return lstOrders;
	}

	public void addOrder(Order o) {
		if(!lstOrders.contains(o)){
			lstOrders.add(o);
		}
	}

	public void removeOrder(Order o) {
		if(lstOrders.contains(o)){
			lstOrders.remove(o);
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("Navn: " + this.name + "\n");
		sb.append("Adresse: " + this.address + "\n");
		sb.append("Postnr.: " + this.zipCode + "\n");
		sb.append("Tlf.: " + this.phone + "\n");
		sb.append("Email: " + this.mail + "\n");

		return sb.toString();
	}


}
