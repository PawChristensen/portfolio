package application.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class TourBooking {
	private LocalDateTime date;
	private OrderLine orderLine;

	public TourBooking(LocalDateTime date, OrderLine orderLine) {
		this.date = date;
		this.orderLine = orderLine;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public OrderLine getOrderLine() {
		return orderLine;
	}

	public void setOrderLine(OrderLine orderLine) {
		this.orderLine = orderLine;
	}


	/* TabelViewMethods */
	
	public Integer getAntal() {		
		return this.getOrderLine().getQuantity();	
	}
	
	public Customer getCustomer() {
		return this.getOrderLine().getOrder().getCustomer();
	}
	
	public LocalDate getShortDate() {
		return this.date.toLocalDate();
	}
	
	public LocalTime getTime() {
		return this.date.toLocalTime();
	}
	
	public Integer getPhone() {
		return this.getOrderLine().getOrder().getCustomer().getPhone();
	}




}
