var getScores = function(){

    $(".firstSixDices").each(function(i){
        i++;
        if (!$(this).prop("disabled")){
            $(this).val(valueSpecificFace(i));
        }
    });
    if (!$("#onePair").prop("disabled")){
        $("#onePair").val(valueOnePair());
    }
    if (!$("#twoPair").prop("disabled")){
        $("#twoPair").val(valueTwoPair());
    }
    if (!$("#threeSame").prop("disabled")){
        $("#threeSame").val(valueThree());
    }
    if (!$("#fourSame").prop("disabled")){
        $("#fourSame").val(valueFour());
    }
    if (!$("#fullHouse").prop("disabled")){
        $("#fullHouse").val(valueFullHouse());
    }
    if (!$("#smallStraight").prop("disabled")){
        $("#smallStraight").val(valueSmallStraight());
    }
    if (!$("#largeStraight").prop("disabled")){
        $("#largeStraight").val(valueLargeStraight());
    }
    if (!$("#chance").prop("disabled")){
        $("#chance").val(valueChance());
    }
    if (!$("#yatze").prop("disabled")){
        $("#yatze").val(valueYatzy());
    }

};

var getSum = function(val){
    var sum = +$("#sum").val();

    var result = sum + val;
    $("#sum").val(result);

    if($("#sum").val() >= 63 && $("#bonus").val() != 50){
        $("#bonus").val(50);
        getTotal(50);
    }


};

var getTotal = function(val){
    var total = +$("#total").val();

    var result = total + val;

    $("#total").val(result);

    //Locking all input fields
    $("#scoreTable").find("input").css("pointer-events","none").css("cursor","no-drop");

    //Resetting throwcount
    resetThrowCount();

    //Enable throw button
    $("#throwDice").prop("disabled", false);

    //Disable checkbox
    $(".diceThrowHeader").find("[type=checkbox]").prop('disabled', true);

    clearResults();
};

function clearResults(){
    $(".diceThrowHeader").find("[type=checkbox]").prop('checked', false);

    $("#resultOne").find(".diceValue").css("border", "none");
    $("#resultOne").find(".diceValue").css("background-color", "unset");
    $("#resultTwo").find(".diceValue").css("border", "none");
    $("#resultTwo").find(".diceValue").css("background-color", "unset");
    $("#resultThree").find(".diceValue").css("border", "none");
    $("#resultThree").find(".diceValue").css("background-color", "unset");
    $("#resultFour").find(".diceValue").css("border", "none");
    $("#resultFour").find(".diceValue").css("background-color", "unset");
    $("#resultFive").find(".diceValue").css("border", "none");
    $("#resultFive").find(".diceValue").css("background-color", "unset");
}

