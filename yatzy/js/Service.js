$(document).ready(function () {

    /* Lock input fields */
    $("[type=text]").prop("readonly", true);

    /* Lets you lock the dice by clicking the image */
    $("#resultOne").find(".diceValue").on('click', function(){
        $("#holdOne").click();
        if($("#holdOne").is(":checked")) {
            $("#resultOne").find(".diceValue").css("border", "1px solid #dddfe2");
            $("#resultOne").find(".diceValue").css("background-color", "#f6f7f9");
        }else{
            $("#resultOne").find(".diceValue").css("border", "none");
            $("#resultOne").find(".diceValue").css("background-color", "unset");
        }
    });
    $("#resultTwo").find(".diceValue").on('click', function(){
        $("#holdTwo").click();
        if($("#holdTwo").is(":checked")) {
            $("#resultTwo").find(".diceValue").css("border", "1px solid #dddfe2");
            $("#resultTwo").find(".diceValue").css("background-color", "#f6f7f9");
        }else{
            $("#resultTwo").find(".diceValue").css("border", "none");
            $("#resultTwo").find(".diceValue").css("background-color", "unset");
        }
    });
    $("#resultThree").find(".diceValue").on('click', function(){
        $("#holdThree").click();
        if($("#holdThree").is(":checked")) {
            $("#resultThree").find(".diceValue").css("border", "1px solid #dddfe2");
            $("#resultThree").find(".diceValue").css("background-color", "#f6f7f9");
        }else{
            $("#resultThree").find(".diceValue").css("border", "none");
            $("#resultThree").find(".diceValue").css("background-color", "unset");
        }
    });
    $("#resultFour").find(".diceValue").on('click', function(){
        $("#holdFour").click();
        if($("#holdFour").is(":checked")) {
            $("#resultFour").find(".diceValue").css("border", "1px solid #dddfe2");
            $("#resultFour").find(".diceValue").css("background-color", "#f6f7f9");
        }else{
            $("#resultFour").find(".diceValue").css("border", "none");
            $("#resultFour").find(".diceValue").css("background-color", "unset");
        }
    });
    $("#resultFive").find(".diceValue").on('click', function(){
        $("#holdFive").click();
        if($("#holdFive").is(":checked")) {
            $("#resultFive").find(".diceValue").css("border", "1px solid #dddfe2");
            $("#resultFive").find(".diceValue").css("background-color", "#f6f7f9");
        }else{
            $("#resultFive").find(".diceValue").css("border", "none");
            $("#resultFive").find(".diceValue").css("background-color", "unset");
        }
    });

    /* Throw dice */
    $("#throwDice").on("click", function () {

        $("#scoreTable").find("input").css("pointer-events","auto").css("cursor","pointer");

        $(".diceThrowHeader").find("[type=checkbox]").prop('disabled', false);

        var holding = [$("#holdOne").is(":checked"), $("#holdTwo").is(":checked"), $("#holdThree").is(":checked"), $("#holdFour").is(":checked"), $("#holdFive").is(":checked")];

        throwDice(holding);

        var result = getValues();

        $("#resultOne").find(".diceValue").css("background-image", "url('img/dices/" + result[0] + ".png')");
        $("#resultTwo").find(".diceValue").css("background-image", "url('img/dices/" + result[1] + ".png')");
        $("#resultThree").find(".diceValue").css("background-image", "url('img/dices/" + result[2] + ".png')");
        $("#resultFour").find(".diceValue").css("background-image", "url('img/dices/" + result[3] + ".png')");
        $("#resultFive").find(".diceValue").css("background-image", "url('img/dices/" + result[4] + ".png')");

        if (getThrowCount() === 3) {
            $(this).prop("disabled", true);
        }
        getScores();
    });

    /* allows you to pick a score field */
    $("#scoreTable input").on("click", function () {
        if (!$(this).hasClass("notClickable")) {
            $(this).prop("disabled", true);
            getTotal(+$(this).val());
        }

        if ($(this).hasClass("firstSixDices")) {
            getSum(+$(this).val());
        }
    });

});